module github.com/an0dize/gopty

go 1.15

require (
	github.com/creack/pty v1.1.11
	github.com/gorilla/websocket v1.4.2
	golang.org/x/crypto v0.0.0-20201016220609-9e8e0b390897
	golang.org/x/sys v0.0.0-20201027140754-0fcbb8f4928c // indirect
)
