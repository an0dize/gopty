package main

import (
	"encoding/json"
	"log"
	"net/http"
	"os"
	"os/exec"

	"github.com/creack/pty"
	"github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

// Payload for sending/receiving data to/from websocket
type Payload struct {
	Auth string `json:"auth"`
	Data string `json:"data"`
}

// StartPutty creates an instance of pty
func StartPutty() (*os.File, *exec.Cmd, error) {
	c := exec.Command("bash")
	//  Start assigns a pseudo-terminal tty os.File to c.Stdin, c.Stdout,
	// and c.Stderr, calls c.Start, and returns the File of the tty's
	// corresponding pty.
	ptmx, err := pty.Start(c)
	if err != nil {
		return nil, nil, err
	}

	return ptmx, c, nil
}

// func putty2() error {
// 	c := exec.Command("bash")
// 	ptmx, err := pty.Start(c)
// 	if err != nil {
// 		return err
// 	}

// 	// close pty at the end
// 	defer func() { _ = ptmx.Close() }()

// 	ch := make(chan os.Signal, 1)
// 	// SIGWINCH is terminal resizing (rows and/or columns)
// 	// SIGINT is terminal interrup (^C)
// 	//notify the channel whenever SIGWINCH signal is detected
// 	signal.Notify(ch, syscall.SIGWINCH)

// 	// whenever channel has input, resize pty
// 	go func() {
// 		for range ch {
// 			if err := pty.InheritSize(os.Stdin, ptmx); err != nil {
// 				log.Printf("error resizing pty: %s", err)
// 			}

// 		}
// 	}()

// 	ch <- syscall.SIGWINCH

// 	// set stdin in raw mode
// 	oldState, err := terminal.MakeRaw(int(os.Stdin.Fd()))
// 	if err != nil {
// 		panic(err)
// 	}
// 	// restore terminal at the end
// 	defer func() { _ = terminal.Restore(int(os.Stdin.Fd()), oldState) }()

// 	// copy stdin to pty (in a go routine, because stdin fd doesn't reach EOF so won't exit on its own.  this will keep passing things in)
// 	go func() { _, _ = io.Copy(ptmx, os.Stdin) }()
// 	_, _ = io.Copy(os.Stdout, ptmx)

// 	return nil

// }

func authenticate(r *http.Request) error {
	return nil
}

func checkOrigin(r *http.Request) bool {
	return true
}

func handler(w http.ResponseWriter, r *http.Request) {
	log.Println("CONNECTION ESTABLISHED")
	upgrader.CheckOrigin = checkOrigin
	if err := authenticate(r); err != nil {
		log.Println(err)
		return
	}
	// upgrade connection to websocket
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}
	//log.Println("0")
	// start putty terminal, returns pseudo-terminal multiplexer
	ptmx, command, err := StartPutty()
	if err != nil {
		log.Fatal(err)
	}

	//log.Println("1")
	// ensure it closes at the end
	defer func() { _ = ptmx.Close(); println("ptmx closed") }()

	//log.Println("2")
	// goroutine for reading from websocket and writing to stdin via ptmx
	go func(c *websocket.Conn, ptymx *os.File) {
		for {
			//log.Println("3")
			_, reader, err := c.NextReader()
			if err != nil {
				log.Println("READER ERROR")
				log.Println(err)
				c.Close()
				return
			}

			//log.Println("3.5")
			inbytes := make([]byte, 1024)
			n, err := reader.Read(inbytes)
			if err != nil {
				log.Println(err)
			}
			log.Println("input: " + string(inbytes[:n]))
			// read from reader into byte buffer
			// inbuf := new(bytes.Buffer)
			// _, err = inbuf.ReadFrom(reader)
			// if err != nil {
			// 	log.Println(err)
			// 	return
			// }
			//log.Println("4")
			// get bytes from buffer, then unmarshal json into `payload` struct
			// inbytes := inbuf.Bytes()
			var payload Payload
			if err := json.Unmarshal(inbytes[:n], &payload); err != nil {
				log.Println(err)
			}
			//log.Println("5")
			// write it to pty
			ptymx.Write([]byte(payload.Data))
			//log.Println("6")
		}
	}(conn, ptmx)

	// goroutine for reading from pty and writing to websocket
	go func(c *websocket.Conn, ptymx *os.File) {
		for {
			//log.Println("7")
			// get next writer
			w, err := conn.NextWriter(websocket.TextMessage)
			if err != nil {
				log.Println("WRITER ERROR")
				log.Println(err)
				return
			}
			//response, err := json.Marshal(&Payload{Data: "hello", Stream: 0})

			outbytes := make([]byte, 1024)
			n, err := ptymx.Read(outbytes)
			if err != nil {
				log.Println(err)
				return
			}

			//outbytes := outbuf.Bytes()
			response, err := json.Marshal(&Payload{Data: string(outbytes[:n])})
			if err != nil {
				log.Println(err)
				return
			}
			log.Println("output: " + string(response))
			//log.Println("9")
			if _, err := w.Write(response); err != nil {
				log.Println(err)
				return
			}

			//log.Println("10")
			if err := w.Close(); err != nil {
				log.Println(err)
				return
			}
			//log.Println("11")
		}
	}(conn, ptmx)

	command.Wait()

	// messageType, reader, err := conn.NextReader()
	// if err != nil {
	// 	log.Println(err)
	// 	return
	// }
	// println("3")
	// // read from reader into byte buffer
	// inbuf := new(bytes.Buffer)
	// _, err = inbuf.ReadFrom(reader)
	// if err != nil {
	// 	log.Println(err)
	// 	return
	// }
	// println("4")
	// // get bytes from buffer, then unmarshal json into `payload` struct
	// inbytes := inbuf.Bytes()
	// var payload Payload
	// if err := json.Unmarshal(inbytes, &payload); err != nil {
	// 	log.Println(err)
	// }
	// println("5")
	// // if payload's stream is stdin(0), write it to pty
	// if payload.Stream == 0 {
	// 	ptmx.Write([]byte(payload.Data))
	// }
	//log.Println(payload)
	// println("6")
	// // get next writer to respond
	// w, err := conn.NextWriter(messageType)
	// if err != nil {
	// 	log.Println(err)
	// 	return
	// }
	// //response, err := json.Marshal(&Payload{Data: "hello", Stream: 0})

	// println("7")
	// outbuf := new(bytes.Buffer)
	// _, err = outbuf.ReadFrom(ptmx)
	// if err != nil {
	// 	log.Println(err)
	// 	return
	// }
	// outbytes := outbuf.Bytes()
	// read
	// ioutil.ReadAll()
	// // Copy ptmx to writer for output
	// if _, err := io.Copy(w, ptmx); err != nil {
	// 	log.Println(err)
	// }
	// if _, err := w.Write(response); err != nil {
	// 	log.Println(err)
	// 	return
	// }
	// if _, err := w.Write(bytes); err != nil {
	// 	log.Println(err)
	// 	return
	// }
	// println("8")
	// if err := w.Close(); err != nil {
	// 	log.Println(err)
	// 	return
	// }
	log.Println("END")
}

func main() {
	log.Println("PROGRAM STARTED")
	http.HandleFunc("/", handler)
	log.Fatal(http.ListenAndServe(":10000", nil))

	// if err := putty(); err != nil {
	// 	log.Fatal(err)
	// }
}
